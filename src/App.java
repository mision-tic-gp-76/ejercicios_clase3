import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        calcular_mayor();
    }

    //ejercicio 4
    public static void calcular_velocidad(){
        try(Scanner vel = new Scanner(System.in)){
            System.out.print("Por favor ingrese la velocidad en km/h: ");
            int num = vel.nextInt();
            float result = (num * 1000)/3600;
            System.out.println("La velocidad en m/s es: "+result+" m/s");

        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Debe digitar un número entero");
        }
    }

    //ejercicio 5
    public static void calcular_hipotenusa(){
        Scanner cat_1 = new Scanner(System.in);
        System.out.print("Por favor ingrese la medida del cateto 1: ");
        float num = cat_1.nextInt();
        Scanner cat_2 = new Scanner(System.in);
        System.out.print("Por favor ingrese la medida del cateto 2: ");
        float num2 = cat_2.nextInt();
        double result = Math.sqrt(Math.pow(num, 2) + Math.pow(num2, 2));
        System.out.println("La medida de la hipotenusa es: "+result);
        cat_1.close();
        cat_2.close();
    }

    //ejercicio 6
    public static void det_multiplo(){
        Scanner cat_1 = new Scanner(System.in);
        System.out.print("Por favor ingrese el número entero: ");
        int num = cat_1.nextInt();
        String resultado = (num%10 == 0) ? num+" es multiplo de 10" :  num+" no es multiplo de 10";
        System.out.println(resultado);
    }

    //ejercicio 8
    public static void division(){
        Scanner cat_1 = new Scanner(System.in);
        System.out.print("Por favor ingrese el 1 entero: ");
        int num = cat_1.nextInt();
        Scanner cat_2 = new Scanner(System.in);
        System.out.print("Por favor ingrese 2 entero: ");
        int num2 = cat_2.nextInt();
        if(num2 != 0){
            double result = num/num2;
            System.out.println("El resultado de la división es: "+result);
        }else{
            System.out.println("ingrese un número distinto de 0");
        }
        
        cat_1.close();
        cat_2.close();
    }

    public static void calcular_mayor(){
        Scanner ent_1 = new Scanner(System.in);
        System.out.print("Por favor ingrese el 1 entero: ");
        int num = ent_1.nextInt();
        Scanner ent_2 = new Scanner(System.in);
        System.out.print("Por favor ingrese 2 entero: ");
        int num2 = ent_2.nextInt();
        Scanner ent_3 = new Scanner(System.in);
        System.out.print("Por favor ingrese 3 entero: ");
        int num3 = ent_3.nextInt();
        int mayor = 0;
        if(num > num2 & num > num3){
            System.out.println(num+" es mayor");
        }else{
            if(num2 > num & num2 > num3){
                System.out.println(num2+" es mayor");
            }else{
                System.out.println(num3+" es mayor");
            }
        }
        ent_1.close();
        ent_2.close();
        ent_3.close();
    }
}
